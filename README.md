Sample python code for generating x509 certificates and VOMS Proxies from
STS. This example is taken from a Django application. In this case, Mellon has 
been set up on the apache server as the SAML2.0 service provider and 
authentication data is included in the request sent to the view getCert.

Resources: 
* [WLCG Federated Access](https://espace.cern.ch/authentication/CERN%20Authentication/WLCG%20Federated%20Access.aspx)
* [Javascript example](https://gitlab.cern.ch/sts/kipper)
* [PHP example](https://gitlab.cern.ch/sts/educert)