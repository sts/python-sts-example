def getCert(request):
    import lxml.etree as ET
    from datetime import datetime
    import requests
    
    ## In this case, a django application, authentication information is 
    ## included in the incoming request.
    ## Extract SAML token
    if ( not ( 'MELLON_SAML_RESPONSE' in request.META)):
        return HttpResponse('No SAML Token', mimetype='text/html')

    saml = request.META.get('MELLON_SAML_RESPONSE').decode('base64')
    samlxml = ET.fromstring(saml)

    ## Get the Assertion out of the token
    assertion = samlxml.find('{urn:oasis:names:tc:SAML:2.0:assertion}Assertion')

    ## Extract the user, for use in debugging
    user = assertion.find(".//{urn:oasis:names:tc:SAML:2.0:assertion}Attribute[@Name='http://schemas.xmlsoap.org/claims/CommonName']/{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue").text

    ## Extract the token ID
    assertion_id = assertion.get('ID')

    ## Extract the expiration date
    conditions = assertion.find('{urn:oasis:names:tc:SAML:2.0:assertion}Conditions')
    expiration = conditions.get('NotOnOrAfter')

    ## Check Token Validity
    expiration_date = datetime.strptime(expiration,'%Y-%m-%dT%H:%M:%S.%fZ')
    current_date = datetime.now()
    if ( expiration_date < current_date ):
        return HttpResponse('Token expired', mimetype='text/html')

    ## In your application, change these to appropriate values
    stsAddress = 'https://aipanda093-sts.cern.ch:8443/sts/wstrust'
    vo='bitface'

    ## Create SOAP Request for STS
    soap_body = """<?xml version="1.0" encoding="UTF-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">
    http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
    <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">
    urn:uuid:99999999-0000-0000-0000-000000000000</wsa:MessageID>
    <wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">
    """+stsAddress+"""</wsa:To>
    <sbf:Framework version="2.0" xmlns:sbf="urn:liberty:sb"/>
    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    <wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
    <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
    """+current_date.strftime('%Y-%m-%dT%H:%M:%S.%fZ')+"""
    </wsu:Created>
    </wsu:Timestamp>
    """+ET.tostring(assertion)+"""
    </wsse:Security>
    </soap:Header>
    <soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <wst:RequestSecurityToken Context="urn:uuid:00000000-0000-0000-0000-000000000000" xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
    <wst:RequestType xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wst:RequestType>
    <wst:TokenType xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">urn:glite.org:sts:GridProxy</wst:TokenType>
    <wst:Claims Dialect="http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0" xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
    <wsse:SecurityTokenReference xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    <wsse:Reference URI="#"""+assertion_id+"""" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
    </wsse:SecurityTokenReference>
    </wst:Claims>
    <gridProxy:GridProxyRequest xmlns:gridProxy="urn:glite.org:sts:proxy" lifetime="86400">
    <gridProxy:VomsAttributeCertificates xmlns:gridProxy="urn:glite.org:sts:proxy">
    <gridProxy:FQAN xmlns:gridProxy="urn:glite.org:sts:proxy">"""+vo+""":/"""+vo+"""</gridProxy:FQAN>
    </gridProxy:VomsAttributeCertificates>
    </gridProxy:GridProxyRequest>
    </wst:RequestSecurityToken>
    </soap:Body>
    </soap:Envelope>"""

    ## Specify the incoming data format and SOAPAction (required) as sts endpoint
    soap_headers = {
        'Content-Type': 'text/xml; charset=utf-8',
        'SOAPAction' : stsAddress
    }

    ## Send SOAP Request
    response = requests.post(stsAddress,data=soap_body,headers=soap_headers, verify=False)
    print(response.content)

    ## Check SOAP Response & extract elements
    sts_response = ET.fromstring(response.content)
    voms_proxy = ''
    certificate  = ''

    for token in sts_response.findall('.//{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}BinarySecurityToken'):
        tokenid = token.get('{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Id')
        if tokenid == '#X509SecurityToken':
            certificate = token.text
        elif tokenid == '#VOMSSecurityToken':
            voms_proxy = token.text
        else :
            return HttpResponse('Unexpected security tokens in STS response', mimetype='text/html')

    key = sts_response.find('.//{http://docs.oasis-open.org/ws-sx/ws-trust/200512}BinarySecret')
    if not len(voms_proxy) or not len(certificate) or key is None :
        return HttpResponse('Unable to generate certificate. Check that user '+user+' is registered in VOMS', mimetype='text/html')

    print('Proxy: '+voms_proxy)
    print('Certificate: '+certificate)
    print('Key: '+key.text)

    ## Store the elements somewhere, to be used when accessing additional services - TODO
    return  HttpResponse(response.content, mimetype='text/html')
